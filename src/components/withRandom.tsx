// Higher Order Component
import { ComponentType, useCallback, useState, useEffect } from "react";

export function withRandom<T>(Component: ComponentType<T>) {
  return (hocProps: Omit<T, "count">) => {
    const [count, setCount] = useState(0);

    useEffect(() => {
      const incrTimer = setInterval(
        () =>
          setCount(Math.floor(Math.random()*100 )),
        1000
      );
    }, []);
    return (
      <Component
        {...(hocProps as T)}
        count={count}
      />
    );
  };
}

// Higher Order Component
const Display = ({count}: {count: number}) => {
  return <p>{count}</p>;
};

export default Display;

// Render prop
import React, { useState } from 'react';

interface CounterProps {
  message: string;
}

const Counter = (props: CounterProps ) => {
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>{props.message}</p>
      <p>You have clicked this message {count} times.</p>
      <button onClick={() => setCount(count + 1)}>
        Click Me!
      </button>
    </div>
  );
};

export default Counter;
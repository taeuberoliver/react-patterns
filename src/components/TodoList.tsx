// presentational and container component
import React from 'react';

interface TodoItemProps {
    task: string;
    onDelete: () => void;
}

const TodoItem = (props: TodoItemProps) => (
    <div>
      {props.task}
      <button onClick={props.onDelete}>Delete</button>
    </div>
  );

class TodoList extends React.Component {
  state = {
    tasks: []
  };

  addTask = (task: string) => {
    this.setState({ tasks: [...this.state.tasks, task] });
  };

  deleteTask = (taskIndex: Number) => {
    this.setState({
      tasks: this.state.tasks.filter((_, index) => index !== taskIndex)
    });
  };

  render() {
    return (
      <div>
        <button onClick={() => this.addTask("New Task")}>Add Task</button>
        {this.state.tasks.map((task, index) => (
          <TodoItem
            key={task}
            task={task}
            onDelete={() => this.deleteTask(index)}
          />
        ))}
      </div>
    );
  }
}

export default TodoList;
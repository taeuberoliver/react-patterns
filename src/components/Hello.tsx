//Provider Pattern
import React, { useState } from 'react';

interface ProviderProps {
    children: React.ReactNode;
}

const UserContext = React.createContext({
    name: '',
    setName: (name: string) => {}
});

const UserProvider = ({ children }: ProviderProps) => {
    const [name, setName] = useState('');

    return (
        <UserContext.Provider value={{ name, setName }}>
            {children}
        </UserContext.Provider>
    );
};

const ChildComponent = () => {
    const { name, setName } = React.useContext(UserContext);

    return (
        <div>
            <h2>Hello, {name}!</h2>
            <input type="text" value={name} onChange={e => setName(e.target.value)} />
        </div>
    );
};

const Hello = () => {
    return (
        <UserProvider>
            <ChildComponent />
        </UserProvider>
    );
};

export default Hello;

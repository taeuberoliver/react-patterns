// State reducer
import React, { useReducer } from 'react';

interface Furniture {
  productNo: string;
  name: string;
  price: number;
  designer: string;
}

const furniture: Furniture[] = [
  {
    productNo: "805.300.05",
    name: "VARMBLIXT",
    price: 34.99,
    designer: "Sabine Marcelis"
  },
  {
    productNo: "905.119.59",
    name: "KONSTFULL",
    price: 4.99,
    designer: "Ilse Crawford"
  },
  {
    productNo: "504.545.07",
    name: "RÅVAROR",
    price: 99,
    designer: "Eva Lilja Löwenhielm"
  },
  {
    productNo: "005.221.13",
    name: "BLÅVINGAD",
    price: 27.99,
    designer: "S Edholm/L Ullenius"
  },
]
const Selector = () => {
  function reducer(state: Furniture, action: string) {
    let newState = furniture.find((f) => f.productNo === action);
    if (newState) {
      return newState;
    }
    return state;
  }
  const [state, dispatch] = useReducer(reducer, furniture[0]);

  return (
    <div>
      <select onChange={e => dispatch(e.target.value)}>
        {furniture.map((f) => (
          <option key={f.productNo} value={f.productNo}>
            {f.name}
          </option>
        ))}
      </select>
      <table>
        <tbody>
          <tr>
            <th>Product No</th>
            <th>{state.productNo}</th>
          </tr>
          <tr>
            <th>Name</th>
            <th>{state.name}</th>
          </tr>
          <tr>
            <th>Price</th>
            <th>{state.price}</th>
          </tr>
          <tr>
            <th>Designer</th>
            <th>{state.designer}</th>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
export default Selector;
import { useState } from 'react'
import TodoList from './components/TodoList'
import Counter from './components/Counter'
import Display from './components/Display'
import { withRandom } from './components/withRandom'
import Selector from './components/Selector'
import './App.css'
import Hello from "./components/Hello";

const DisplayRandom = withRandom(Display);

function App() {

  return (
    <div className="App">
      <TodoList/>
        <br/>
      <Counter message="Hello React"/>
        <br/>
      <Display count={10} />
      <DisplayRandom />
        <br/>
      <Selector/>
        <br/>
      <Hello/>
    </div>
  )
}

export default App
